# Faclon_Intern_Project

Friend Request API collection. 

## Getting Started

To run the application on your local machine you need to have node.js and mongodb installed, up and running. Once you have installed node.js and mongodb, you can now download this repository as .zip and extract it into a folder. After extracting, open the repository directory in a terminal / cmd and run the following command:

```
npm install
```

Application should now be up and running at port 3000. To access the application simply open your preffered browser and type the following in the url bar:

```
http://localhost:3000
```

To stop the server press Ctrl + C on the server terminal.

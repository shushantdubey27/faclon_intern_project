const Router = require('express').Router();
const controller = require('../controller/controller')
const { validationResult, check } = require('express-validator');
const validator = require('../middleware/validation.middleware');

Router.post(
    '/user', 
    check('email').isEmail(),
    check('name').isLength({ min: 3}),
    validator,
    controller.createUser
);

Router.patch(
    '/user/:id',
    check('name').isLength({ min: 3}),
    check('email').isEmail(),
    check('id').isMongoId(),
    validator,
    controller.updateUser
);

Router.get(
    '/user/:id',
    check('id').isMongoId(),
    validator,
    controller.getUser
);

Router.delete(
    '/user/:id',
    check('id').isMongoId(),
    validator,
    controller.deleteUser
);


//----------------------------


Router.post(
    '/user/:id/friend',
    check('id').isMongoId(),
    check('rId').isMongoId(),
    validator,
    controller.sendReq
);

Router.patch(
    '/user/:id/friend',
    check('id').isMongoId(),
    check('docId').isMongoId(),
    validator,
    controller.statusReq
);

Router.get(
    '/user/:id/friend',
    check('id').isMongoId(),
    validator,
    controller.getFriendList
);

Router.delete(
    '/user/:id/friend',
    check('id').isMongoId(),
    validator,
    controller.userDelete
);


module.exports = Router;